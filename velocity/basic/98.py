#%matplotlib inline
import matplotlib.pyplot as plt
import numpy as np
import math
import sys
import inspect

file="out.98"
with open(file, "r") as ins:
    array = []
    for line in ins:
        temp= [float(x) for x in line.split()]
        array.append(temp)
        
# print array[0][1]
# print 'number of lines in '+file+' is: '+str(len(array))

# the unmodified grid
numGrid=0
while array[numGrid][0] == array[numGrid+1][0]:
	numGrid=numGrid+1
numGrid+=1    
# print 'number of Grids: '+str(numGrid)

numTimeStep=len(array)/numGrid
# print 'number of tStep: '+str(numTimeStep)

# prepare a20,Re_a22,Im_a22
a20=[row[3] for row in array]
Re_a22=[row[5] for row in array]
Im_a22=[row[1] for row in array]


def pltEigenVector():
# X -> y, Y -> t
	X,Y = np.meshgrid(np.arange(0, len(array)/numGrid,1 ), np.arange(0,numGrid,1))

	# a20=[row[3] for row in array]
	# Re_a22=[row[5] for row in array]
	# Im_a22=[row[1] for row in array]

	Qxx=[]
	Qyy=[]
	Qxy=[]
	for i in range(len(a20)):
		Qxx.append(-2/3*math.sqrt(math.pi/5)*a20[i]+math.sqrt(8*math.pi/15)*Re_a22[i])
		Qxy.append(-math.sqrt(8*math.pi/15)*Im_a22[i])
		Qyy.append(-2/3*math.sqrt(math.pi/5)*a20[i]-math.sqrt(8*math.pi/15)*Re_a22[i])

	# print len(Qxx),len(Qyy),len(Qxy)

	x=[]
	y=[]
	angle=[]
	for i in range(len(Qyy)):
		x.append(-1*(-Qxx[i] + Qyy[i] 
			- math.sqrt(Qxx[i]*Qxx[i] + 4*Qxy[i]*Qxy[i] - 2*Qxx[i]*Qyy[i] + Qyy[i]*Qyy[i])
		)        )
		y.append(2*Qxy[i])

	#**********fix the order for quiver plot****************
	tempU = [0] * len(x)
	tempV = [0] * len(y)

	it=0
	for t in range(0,numTimeStep):
		counter=t
		for n in range(0,numGrid):
			# print it,counter
			tempU[counter]=x[it]
			tempV[counter]=y[it]
			it=it+1
			counter=counter+numTimeStep
	U=tempU
	V=tempV
	#**********normalize the plot****************

	UV=zip(U, V)
	# print len(UV)
	# UV= [x/np.linalg.norm(x) for x in UV]
	for i in range(len(UV)):
		if np.linalg.norm(UV[i])!=0:
			UV[i]=UV[i]/np.linalg.norm(UV[i])

	U=[uv[0]for uv in UV]
	V=[uv[1]for uv in UV]
	# print "U[5]==UV[5][0] in eigenVector.py"
	# print U[5]==UV[5][0]
	#**********normalize the plot****************
	# 1
	plt.figure()
	Q = plt.quiver(X, Y,U, V,
	               pivot='mid', color='r', units='width')

	l, r, b, t = plt.axis()
	dx, dy = r - l, t - b

	plt.axis([l - 0.05*dx, r + 0.05*dx, b - 0.05*dy, t + 0.05*dy])
	plt.xlabel('time')
	plt.ylabel('grid')
	plt.title(inspect.stack()[0][3])

	# save to eps and png
	filename='.'+inspect.stack()[0][3]
	plt.legend(loc='best')			
	# plt.savefig('./t'+str(len(array)/numGrid)+filename+'.eps', format='eps', dpi=1000)
	plt.savefig('./t'+str(len(array)/numGrid)+filename+'.png')
	# print "saved as "+'./t'+str(len(array)/numGrid)+filename

def pltEigenAngle():

	# # prepare a20,Re_a22,Im_a22
	# a20=[row[3] for row in array]
	# Re_a22=[row[5] for row in array]
	# Im_a22=[row[1] for row in array]

	# calculate Q
	Qxx=[]
	Qyy=[]
	Qxy=[]
	for i in range(len(a20)):
		Qxx.append(-2/3*math.sqrt(math.pi/5)*a20[i]+math.sqrt(8*math.pi/15)*Re_a22[i])
		Qxy.append(-math.sqrt(8*math.pi/15)*Im_a22[i])
		Qyy.append(-2/3*math.sqrt(math.pi/5)*a20[i]-math.sqrt(8*math.pi/15)*Re_a22[i])
	# print len(Qxx),len(Qyy),len(Qxy)
	#numGrid of grid points in the last time step  
	Qxx=Qxx[-numGrid:]
	Qxy=Qxy[-numGrid:]
	Qyy=Qyy[-numGrid:]
	# print len(Qxx),len(Qyy),len(Qxy)

	# prepare for the eigen vector <x,y>
	x=[]
	y=[]
	angle=[]
	for i in range(len(Qyy)):
		x.append(-1*(-Qxx[i] + Qyy[i] 
			- math.sqrt(Qxx[i]*Qxx[i] + 4*Qxy[i]*Qxy[i] - 2*Qxx[i]*Qyy[i] + Qyy[i]*Qyy[i])
		))
		y.append(2*Qxy[i])
		# print x[i],y[i]

	for i in range(len(Qyy)):
		if x[i]!=0:
			angle.append(y[i]/x[i])
		else:
			angle.append(y[i]/sys.float_info.min)

	angle=abs(np.arctan(angle))
	for j in range(len(angle)):
		if angle[j]<0:
			angle[j]=angle[j]+math.pi

	angle=[math.degrees(x) for x in angle]

	plt.figure()
	plt.plot(np.linspace(0,1,len(angle)),angle)
	l, r, b, t = plt.axis()
	dx, dy = r - l, t - b

	plt.axis([l - 0.05*dx, r + 0.05*dx, b - 0.05*dy, t + 0.05*dy])
	plt.xlabel('time')
	plt.ylabel('grid')
	# plt.title(inspect.stack()[0][3], loc='right')
	
	with open("fort.95", "r") as ins:
	    parameters = []
	    for line in ins:
	    	lineTrimmed=line.rstrip('\n').rstrip('\r')
	        parameters.append(lineTrimmed)

	plt.title(' PN='+parameters[1].split()[0]
		+', PAL='+parameters[1].split()[1]
		+', PDS='+parameters[2].split()[1]
		+', PCT='+parameters[2].split()[2]
		+', PV='+parameters[2].split()[3],  fontsize=16,loc='left')

	# save to eps and png
	filename='.'+inspect.stack()[0][3]
	plt.legend(loc='best')			
	# plt.savefig('./t'+str(len(array)/numGrid)+filename+'.eps', format='eps', dpi=1000)
	plt.savefig('./t'+str(len(array)/numGrid)+filename+'.png')
	# print "saved as "+'./t'+str(len(array)/numGrid)+filename



if __name__ == '__main__':
	# print(sys.argv, len(sys.argv))

	if len(sys.argv)-1 ==0:
		pltEigenAngle()
		pltEigenVector()
#		plt.show()
		
	else:
		print "input wrong in "+sys.argv[0]
