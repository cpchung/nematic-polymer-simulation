#!/bin/bash
#  	0
#  2.0    -1    8.0    1.0   0.950
#    2.000     0.2    -4.000     1.000    40.0	2E06	1
# 5E-04    0   .1E-09
#    5.000     5.000     5.000     4.000     1.000
#   1.00  0.00  1.00     -1.0 
#     READ(95,*) RESTART
#     READ(95,*) PN,PAL,PN1,PU0,PA
#     READ(95,*) PDe,PDS,PCT,PV,FTIME,NKIT,CODE
#     READ(95,*) NTS, MSTEP, EPS
#     READ(95,*) ETA, Pre2, Pre3, PG, AL0
#     READ(95,*) CO2, CT2, PMGF
bash killAll.sh
lsof +D *.folder | awk '{print $2}' | tail -n +2 | xargs kill -9
rm -rf *.folder

RESTART=0
PA=0.95
FTIME=40.0;NKIT=2E06;CODE=1
NTS=5E-04; MSTEP=0; EPS=1E-09
ETA=5.000; Pre2=5.000;Pre3=5.000;PG=4.000;AL0=1.000
CO2=1.00;CT2=0.00;PMGF=1.00

counter=0

for ((PN=2; PN<=8; PN=PN+3)); do
	for ((PAL=-1; PAL<=-1; PAL=PAL+1)); do
		for ((PN1=8; PN1<=8; PN1=PN1+2)); do
			for ((PU0=1; PU0<=1; PU0=PU0+2)); do
				# for ((PA= 0.95; PA<= 0.95; PA=PA+2)); do
				for PDe in 1; do
					for PDS in 0.2 0.02 0.002 0.0002; do
						# for ((PDS=0.2; PDS<=0.2; PDS=PDS+2)); do
							for ((PCT= -12; PCT<= -2; PCT=PCT+3)); do    
								# for ((PV=2; PV<=8; PV=PV+2)); do
								for PV in 10 13 16; do
									# echo 'mkdir ',$counter
									mkdir $counter.folder
									cp ./basic/* $counter.folder
									# echo 'entering ',$counter.folder
									cd $counter.folder

									echo $RESTART > $counter.95
									echo $PN $PAL $PN1 $PU0 $PA >> $counter.95
									echo $PDe $PDS $PCT'E+0' $PV $FTIME $NKIT $CODE >> $counter.95
									echo $NTS $MSTEP $EPS >> $counter.95
									echo $ETA $Pre2 $Pre3 $PG $AL0 >> $counter.95
									echo $CO2 $CT2 $PMGF >> $counter.95
									# subl $counter.95
									cp $counter.95 fort.95
									bash qsub.sh
									cd ..
									# echo 'back '
									counter=$((counter = $counter + 1))
									

								done
							done
						# done
					done
				 done
			done
		done
	done
done
echo $counter, 'experiments are created'
bash check.sh

