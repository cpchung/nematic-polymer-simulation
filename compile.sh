gfortran -o acp.o -c -g -O3 -ffast-math acp.f90
gfortran -o vel.o -c -g -O3 -ffast-math vel.f90
gfortran -o sf.o -c -g -O3 -ffast-math sf.f90
gfortran -o fv.o -c -g -O3 -ffast-math fv.f90
gfortran -o matrix.o -c -g -O3 -ffast-math matrix.f90
gfortran -o method.o -c -g -O3 -ffast-math method.f90
gfortran -o main.o -c -g -O3 -ffast-math main.f90
gfortran -o aa -g main.o acp.o fv.o matrix.o method.o sf.o vel.o