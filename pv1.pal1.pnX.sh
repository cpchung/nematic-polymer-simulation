gfortran -Wall -Wextra -Wconversion -pedantic -C main.f90 acp.f90 fv.f90 matrix.f90 method.f90 sf.f90 |& grep . > Wall_Wextra_message
# gfortran -C main.f90 acp.f90 fv.f90 matrix.f90 method.f90 sf.f90 |& grep . > O3_message
gfortran -O3 main.f90 acp.f90 fv.f90 matrix.f90 method.f90 sf.f90 |& grep . > O3_message

# define variable
anchor=".norAnch"
png=".png"
eps=".eps"
txt=".txt"
gridSize=65



parameters="pv1.pal1.pnX"
firstRun=2
runSize=4
runStep=2
for (( i=$firstRun; i <= $runSize; i=i+$runStep ))
	do echo $i
		suffix=${parameters//[X]/$i}
		echo $suffix

		cp ./fort.95.$suffix$txt ./fort.95

		COMMAND="time ./a.out"
		eval $COMMAND;
		# time $ eval $COMMAND;

# Parse the fort.97, calculate the number of lines from fort.97
		wc fort.97 > xxx
		read lines words characters filename < xxx 
		echo "lines=$lines words=$words characters=$characters filename=$filename"
		
		numTimeStep=$((lines/gridSize))
		numTimeStep=$(($numTimeStep >20?20:$numTimeStep))

		# take the last 20 time steps(20*65=1300,20*64=1280) to plot
		tail -1300 fort.97 > out.97
		mv out.97 fort.97
		python orientVector.py
		# save away the result
		mv t$numTimeStep.97$png t$numTimeStep.97.$suffix$anchor$png
		mv t$numTimeStep.97$eps t$numTimeStep.97.$suffix$anchor$eps
		mv fort.97 fort.97.$suffix$anchor

# Parse the fort.98, calculate the number of lines from fort.98
		wc fort.98 > xxx
		read lines words characters filename < xxx 
		echo "lines=$lines words=$words characters=$characters filename=$filename"
		
		# take the last 20 time steps(20*65=1300,20*64=1280) to plot
		tail -1280 fort.98 > out.98
		mv out.98 fort.98
		python eigenVector.py
		# save away the result
		mv t$numTimeStep.98$png t$numTimeStep.98.$suffix$anchor$png
		mv t$numTimeStep.98$eps t$numTimeStep.98.$suffix$anchor$eps
		mv fort.98 fort.98.$suffix$anchor

done

python arctanN.py fort.97.$parameters$anchor $firstRun $runSize $runStep
python eigenAngleN.py
