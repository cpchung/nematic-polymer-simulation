# gfortran -o nantest -Wextra -Wconversion -pedantic -C main.f90 acp.f90 fv.f90 matrix.f90 method.f90 sf.f90 -ffpe-trap=invalid,zero,overflow -g -static
# gdb ./nantest

gfortran -Wall -Wextra -Wconversion -pedantic -C main.f90 acp.f90 fv.f90 matrix.f90 method.f90 sf.f90 |& grep . > Wall_Wextra_message
# gfortran -C main.f90 acp.f90 fv.f90 matrix.f90 method.f90 sf.f90 |& grep . > O3_message
gfortran -O3 main.f90 acp.f90 fv.f90 matrix.f90 method.f90 sf.f90 |& grep . > O3_message
# ./nantest
time ./a.out